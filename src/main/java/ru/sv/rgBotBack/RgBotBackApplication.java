package ru.sv.rgBotBack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RgBotBackApplication {


    private static Logger logger = LoggerFactory.getLogger(RgBotBackApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(RgBotBackApplication.class, args);
    }

}
